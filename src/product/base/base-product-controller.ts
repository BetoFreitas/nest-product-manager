import { BaseController } from '../../_base/controller/base.controller';
import { Product } from '../entities/product';
import { ProductCreateDto } from '../entities/product-create.dto';
import { ProductUpdateDto } from '../entities/product-update-dto';

export class BaseProductController extends BaseController<Product, ProductCreateDto, ProductUpdateDto> {}
