import { BaseProductService } from './base/base-product-service';
import { DatabaseService } from '../_base/database/database.service';
import { Module } from '@nestjs/common';
import { ProductController } from './product.controller';
import { ProductService } from './product.service';

@Module({
  controllers: [ProductController],
  providers: [
    DatabaseService,
    {
      provide: BaseProductService,
      useClass: ProductService,
    },
  ],
})
export class ProductModule {}
