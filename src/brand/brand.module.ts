import { BaseBrandService } from './base/base-brand-service';
import { BrandController } from './brand.controller';
import { BrandService } from './brand.service';
import { DatabaseService } from '../_base/database/database.service';
import { Module } from '@nestjs/common';

@Module({
  controllers: [BrandController],
  providers: [
    DatabaseService,
    {
      provide: BaseBrandService,
      useClass: BrandService,
    },
  ],
})
export class BrandModule {}
