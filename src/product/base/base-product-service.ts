import { BaseService } from '../../_base/service/base-service';
import { Product } from '../entities/product';
import { ProductCreateDto } from '../entities/product-create.dto';
import { ProductUpdateDto } from '../entities/product-update-dto';

export class BaseProductService extends BaseService<Product, ProductCreateDto, ProductUpdateDto> {}
