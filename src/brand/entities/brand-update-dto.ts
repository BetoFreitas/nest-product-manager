import { IsString } from 'class-validator';

export class BrandUpdateDto {
  @IsString()
  name?: string;

  @IsString()
  code?: string;
}
