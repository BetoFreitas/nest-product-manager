import { BadRequestException, Body, Controller, Post } from '@nestjs/common';
import { ProductCreateDto } from './entities/product-create.dto';
import { BaseProductController } from './base/base-product-controller';

@Controller('product')
export class ProductController extends BaseProductController {
  @Post()
  async create(@Body() entity: ProductCreateDto) {
    try {
      return await this.baseService.create(entity);
    } catch (error) {
      throw new BadRequestException();
    }
  }
}
