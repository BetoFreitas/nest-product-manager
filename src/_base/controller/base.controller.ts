import { BaseService } from '../service/base-service';
import { BadRequestException, Body, Delete, Get, Param, ParseIntPipe, Patch, Post } from '@nestjs/common';

export abstract class BaseController<TEntity, TCreateEntity, TUpdateEntity> {
  constructor(protected baseService: BaseService<TEntity, TCreateEntity, TUpdateEntity>) {}

  @Get()
  async findAll() {
    try {
      return await this.baseService.findAll();
    } catch (error) {
      throw new BadRequestException();
    }
  }

  @Get(':id')
  async find(@Param('id', ParseIntPipe) id: number) {
    try {
      return await this.baseService.find(id);
    } catch (error) {
      throw new BadRequestException();
    }
  }

  @Post()
  async create(@Body() entity: TCreateEntity) {
    try {
      return await this.baseService.create(entity);
    } catch (error) {
      throw new BadRequestException();
    }
  }

  @Patch(':id')
  async update(@Param('id', ParseIntPipe) id: number, @Body() entity: TUpdateEntity) {
    try {
      return await this.baseService.update(id, entity);
    } catch (error) {
      throw new BadRequestException();
    }
  }

  @Delete(':id')
  async remove(@Param('id', ParseIntPipe) id: number) {
    try {
      return await this.baseService.remove(id);
    } catch (error) {
      throw new BadRequestException();
    }
  }
}
