import { BaseBrandService } from './base/base-brand-service';
import { BrandCreateDto } from './entities/brand-create.dto';
import { BrandUpdateDto } from './entities/brand-update-dto';
import { DatabaseService } from '../_base/database/database.service';
import { Injectable } from '@nestjs/common';

@Injectable()
export class BrandService implements BaseBrandService {
  constructor(protected db: DatabaseService) {}

  async findAll() {
    return await this.db.brand.findMany();
  }
  async find(id: number) {
    return await this.db.brand.findFirst({
      where: {
        id,
      },
    });
  }
  async create(entity: BrandCreateDto) {
    return await this.db.brand.create({
      data: entity,
    });
  }
  async update(id: number, entity: BrandUpdateDto) {
    return await this.db.brand.update({
      data: entity,
      where: {
        id,
      },
    });
  }
  async remove(id: number) {
    await this.db.brand.delete({
      where: {
        id,
      },
    });
  }
}
