export abstract class BaseService<TEntity, TCreateEntity, TUpdateEntity = TCreateEntity> {
  findAll: () => Promise<TEntity[]>;
  find: (id: number) => Promise<TEntity | null>;

  create: (entity: TCreateEntity) => Promise<TEntity>;
  update: (id: number, entity: TUpdateEntity) => Promise<TEntity>;

  remove: (id: number) => Promise<void>;
}
