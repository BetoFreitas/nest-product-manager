import { BaseBrandController } from './base/base-brand-controller';
import { Controller } from '@nestjs/common';

@Controller('brand')
export class BrandController extends BaseBrandController {}
