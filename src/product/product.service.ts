import { BaseProductService } from './base/base-product-service';
import { DatabaseService } from '../_base/database/database.service';
import { Injectable } from '@nestjs/common';
import { ProductCreateDto } from './entities/product-create.dto';
import { ProductUpdateDto } from './entities/product-update-dto';

@Injectable()
export class ProductService implements BaseProductService {
  constructor(protected db: DatabaseService) {}

  async findAll() {
    return await this.db.product.findMany({
      include: {
        brand: true,
      },
    });
  }
  async find(id: number) {
    return await this.db.product.findFirst({
      where: {
        id,
      },
      include: {
        brand: true,
      },
    });
  }
  async create(entity: ProductCreateDto) {
    const { brandId, ...product } = entity;

    return await this.db.product.create({
      data: {
        ...product,
        brand: {
          connect: {
            id: brandId,
          },
        },
      },
      include: {
        brand: true,
      },
    });
  }
  async update(id: number, entity: ProductUpdateDto) {
    return await this.db.product.update({
      data: entity,
      where: {
        id,
      },
      include: {
        brand: true,
      },
    });
  }
  async remove(id: number) {
    await this.db.product.delete({
      where: {
        id,
      },
    });
  }
}
