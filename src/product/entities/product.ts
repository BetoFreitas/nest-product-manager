import { Brand } from '../../brand/entities/brand';

export class Product {
  name: string;
  code: string;
  price: number;
  stock: number;
  brand: Brand;
}
