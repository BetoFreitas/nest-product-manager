import { BrandModule } from './brand/brand.module';
import { Module } from '@nestjs/common';
import { ProductModule } from './product/product.module';

@Module({
  imports: [ProductModule, BrandModule],
})
export class AppModule {}
