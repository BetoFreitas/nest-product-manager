import { BaseController } from '../../_base/controller/base.controller';
import { Brand } from '../entities/brand';
import { BrandCreateDto } from '../entities/brand-create.dto';
import { BrandUpdateDto } from '../entities/brand-update-dto';

export class BaseBrandController extends BaseController<Brand, BrandCreateDto, BrandUpdateDto> {}
