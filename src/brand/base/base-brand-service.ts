import { BaseService } from '../../_base/service/base-service';
import { Brand } from '../entities/brand';
import { BrandCreateDto } from '../entities/brand-create.dto';
import { BrandUpdateDto } from '../entities/brand-update-dto';

export class BaseBrandService extends BaseService<Brand, BrandCreateDto, BrandUpdateDto> {}
