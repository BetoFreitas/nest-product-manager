import { IsNumber, IsString, Min } from 'class-validator';

export class ProductUpdateDto {
  @IsString()
  name?: string;

  @IsString()
  code?: string;

  @IsNumber({
    maxDecimalPlaces: 2,
  })
  @Min(0.01)
  price?: number;

  @IsNumber()
  @Min(0)
  stock?: number;

  @IsNumber()
  @Min(0)
  brandId?: number;
}
